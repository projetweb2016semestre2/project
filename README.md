# TP Express #

### Install ###

* npm install

### Run ###

* npm start
* ou
* nodemon src/app.coffee

### Tests ###

* npm test
* ou
* ./bin/test

### Contributors ###

* Rina Attieh